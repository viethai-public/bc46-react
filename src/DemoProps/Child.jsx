import React from 'react'

const Child = (props) => {
    const { data, sum } = props
    console.log('data: ', data)
    const total = sum()
    console.log('total: ', total)
    return <div>Child</div>
}

export default Child
