//rcc
import React, { Component } from 'react'

export default class Lifecycle extends Component {

    // LIFE CYCLE chỉ có ở trong class component
    // Có 3 giai đoạn: 

    // MOUNTING: Các phương thức ở mounting sẽ đc chạy khi component render ra UI
    // constructor: Dùng khởi tạo state và props, dùng để bind this vào các phương thức

    // UPDATING


    // UNMOUNTING


    state = {
        number: 1,
    }

    render() {

        const props = this.props

        return (
            <div>
                <h1>Lifecycle</h1>
                <p>Number {this.state.number}</p>
                <button
                    className="btn btn-success mt-3"
                    onClick={() => {
                        this.setState({
                            number: this.state.number + 1,
                        })
                    }}
                >
                    + Number
                </button>
            </div>
        )
    }
}
