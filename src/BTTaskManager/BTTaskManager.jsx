import React, { useEffect, useState } from 'react'
import axios from 'axios'
import cn from 'classnames'
import { toast } from 'react-toastify'
import { btTaskManagerServices } from '../services/btTaskManagerService'

const BTTaskManager = () => {
    const [taskList, setTaskList] = useState()
    console.log('taskList: ', taskList)

    const [formData, setFormData] = useState()

    // callback (ES5) , promise (ES6), async await(ES7)
    // chỉ sử dụng async await đối với 1 func xử lý bất đồng bộ (await Promise)
    // return trong async await luôn luôn promise

    // const fetchTaskList = async () => {
    //     try {
    //         // lấy data từ api
    //         const data = await axios.get('http://svcy.myclass.vn/api/ToDoList/GetAllTask')
    //         console.log('data: ', data)

    //         // sau khi có data từ api
    //         setTaskList(data.data)
    //     } catch (err) {
    //         console.log(err)
    //     }
    // }

    // const value = fetchTaskList()

    const handleFormData = () => (ev) => {
        const { name, value } = ev.target

        setFormData({
            ...formData,
            [name]: value,
        })
    }

    // chỉ chạy duy nhất 1 lần khi component render xong
    useEffect(() => {
        // fetchTaskList()

        // IIFE
        ;(async () => {
            try {
                // lấy data từ api
                // const data = await axios.get('http://svcy.myclass.vn/api/ToDoList/GetAllTask')
                const data = await btTaskManagerServices.getTaskList()
                console.log('data: ', data)

                // sau khi có data từ api
                setTaskList(data.data)
            } catch (err) {
                console.log(err)
            }
        })()
    }, [])

    return (
        <div>
            <h1>BTTaskManager</h1>
            <div>
                <form
                    onSubmit={async (ev) => {
                        try {
                            // ngăn trình duyệt reload khi submit
                            ev.preventDefault()

                            // Call api tạo mới task
                            // console.log(formData)
                            // await axios.post(
                            //     'http://svcy.myclass.vn/api/ToDoList/AddTask',
                            //     formData
                            // )
                            await btTaskManagerServices.createTask(formData)

                            // sau khi call thành công gọi lại api fetch task list
                            // const data = await axios.get(
                            //     'http://svcy.myclass.vn/api/ToDoList/GetAllTask'
                            // )
                            const data = await btTaskManagerServices.getTaskList()

                            // sau khi có data từ api
                            setTaskList(data.data)
                            toast.success('Create task success!')
                        } catch (err) {
                            toast.error('Create task error!')
                            console.log(err)
                        }
                    }}
                >
                    <input name="taskName" className="form-control" onChange={handleFormData()} />
                    <div>
                        <button className="btn btn-outline-success my-3">Create Task</button>
                    </div>
                </form>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Task Name</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {taskList?.map((task) => (
                            <tr>
                                <td>{task.taskName}</td>
                                <td>
                                    <p
                                        className={cn('badge', {
                                            'badge-danger': !task?.status,
                                            'badge-success': task.status,
                                        })}
                                    >
                                        {task?.status ? 'Done' : 'Inprocessing'}
                                    </p>
                                </td>
                                <td>
                                    {!task?.status && (
                                        <button
                                            className="btn btn-success"
                                            onClick={async () => {
                                                try {
                                                    // update status task => done
                                                    // await axios.put(
                                                    //     `http://svcy.myclass.vn/api/ToDoList/doneTask?taskName=${task.taskName}`
                                                    // )
                                                    await btTaskManagerServices.updateStatusTask(
                                                        task.taskName
                                                    )

                                                    // get lại danh sách task
                                                    // const data = await axios.get(
                                                    //     'http://svcy.myclass.vn/api/ToDoList/GetAllTask'
                                                    // )

                                                    const data =
                                                        await btTaskManagerServices.getTaskList()

                                                    // sau khi có data từ api
                                                    setTaskList(data.data)

                                                    toast.success('Update task success!')
                                                } catch (err) {
                                                    toast.error('Update task error!')
                                                }
                                            }}
                                        >
                                            done
                                        </button>
                                    )}
                                    <button
                                        className="btn btn-danger ml-3"
                                        onClick={async () => {
                                            try {
                                                // deletetask
                                                // await axios.delete(
                                                //     `http://svcy.myclass.vn/api/ToDoList/deleteTask?taskName=${task.taskName}`
                                                // )

                                                await btTaskManagerServices.deleteTask(
                                                    task.taskName
                                                )

                                                // delete thành công call api get lại danh sách task
                                                // const data = await axios.get(
                                                //     'http://svcy.myclass.vn/api/ToDoList/GetAllTask'
                                                // )

                                                const data = btTaskManagerServices.getTaskList()

                                                // sau khi có data từ api
                                                setTaskList(data.data)

                                                toast.success('Delete task success!')
                                            } catch (err) {
                                                toast.error('Delete task error!')
                                            }
                                        }}
                                    >
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                {/* IIFE để render UI */}
                {/* {(() => {
                    const name = 'abc'
                    if (name) {
                        return <div>{name}</div>
                    }

                    return <div> No Name</div> 
                })()} */}
            </div>
        </div>
    )
}

export default BTTaskManager
