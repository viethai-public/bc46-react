//rafce
import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

const DemoRedux = () => {
    // useSelector: dùng để lấy giá trị ở trên store của redux
    // const demoRedux = useSelector((state) => state.demoRedux)
    const { number, name } = useSelector((state) => state.demoRedux)
    // console.log('demoRedux: ', demoRedux)
    
    const dispatch = useDispatch()

    return (
        <div className="container mt-5">
            <h1>DemoRedux</h1>

            <p className="display-4">Number: {number}</p>
            <p className="display-4">Name: {name}</p>
            <div>
                <button
                    className="btn btn-success"
                    onClick={() => {
                        dispatch({
                            type: 'INCREA_NUMBER',
                            payload: 2,
                        })
                    }}
                >
                    Increa Number
                </button>
            </div>
        </div>
    )
}

export default DemoRedux
