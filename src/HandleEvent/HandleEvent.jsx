import React from 'react'

const HandleEvent = () => {
    const showMessage = () => {
        alert('Message Alert')
    }

    const showMessageParam = (mess) => {
        alert(mess)
    }

    return (
        <div className="container">
            HandleEvent
            <div>
                {/* Trường hợp hàm ko có tham số */}
                <button className="btn btn-success mt-3" onClick={showMessage}>
                    Show Message
                </button>

                {/* TRường hợp hàm có tham số */}
                <button
                    className="btn btn-info mt-3 ml-3"
                    onClick={() => showMessageParam('Hello')}
                >
                    Show Message Param
                </button>

                <button
                    className="btn btn-danger mt-3 ml-3"
                    onClick={() => {
                        alert('Click me')
                    }}
                >
                    Click
                </button>
            </div>
        </div>
    )
}

export default HandleEvent
