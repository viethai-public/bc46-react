import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { chairBookingsAction, payAction } from '../store/BTDatve/actions'

const Result = () => {
    const { chairBookings, chairBookeds } = useSelector((state) => state.btDatVe)
    const dispatch = useDispatch()
    console.log('chairBookeds: ', chairBookeds);
    console.log('chairBookings: ', chairBookings)
    return (
        <div>
            <h2>Danh sách ghế bạn chọn</h2>
            <div className="d-flex flex-column">
                <button className="btn btn-outline-dark Chair booked">Ghế đã đặt</button>
                <button className="btn btn-outline-dark mt-3 Chair booking">Ghế đang chọn</button>
                <button className="btn btn-outline-dark mt-3">Ghế chưa đặt</button>
            </div>

            <table className="table">
                <thead>
                    <tr>
                        <th>Số ghế</th>
                        <th>Giá</th>
                        <th>Hủy</th>
                    </tr>
                </thead>
                <tbody>
                    {chairBookings.map((ghe) => (
                        <tr>
                            <td>{ghe.soGhe}</td>
                            <td>{ghe.gia}</td>
                            <td>
                                <button
                                    className="btn btn-danger"
                                    onClick={() => {
                                        dispatch(chairBookingsAction(ghe))
                                    }}
                                >
                                    Hủy
                                </button>
                            </td>
                        </tr>
                    ))}

                    {/* Tính tổng tiền */}
                    <tr>
                        <td>Tổng tiền</td>
                        <td>{chairBookings.reduce((total, ghe) => (total += ghe.gia), 0)}</td>
                    </tr>
                </tbody>
            </table>

            <button
                className="btn btn-success"
                onClick={() => {
                    dispatch(payAction())
                }}
            >
                Thanh toán
            </button>
        </div>
    )
}

export default Result
