
import qs from 'qs'

/**
 * @param {*} object 
 * @returns search string dùng để query URL
 */

export const generateSearchString = (params) => {
    const searchString = qs.stringify(params, {
        addQueryPrefix: true,
    })

    return searchString
}
