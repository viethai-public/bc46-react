import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    number: 100,
    chairBookings: [],
    chairBookeds: [],
}

const BTDatVeSlice = createSlice({
    name: 'BTDatVe',
    initialState,
    reducers: {
        setNumber: (state, action) => {
            console.log('action: ', action)
            state.number = action.payload
        },
        setChairBookings: (state, action) => {
            // Kiểm tra có tồn tại ghế trong mảng chairbooking
            const index = state.chairBookings.findIndex((e) => e.soGhe === action.payload.soGhe)

            if (index !== -1) {
                // nếu như đã tồn tại=> xóa khỏi mảng chairbookings
                state.chairBookings.splice(index, 1)
            } else {
                // nếu chưa tồn tại=> push vào mảng chairbookings
                state.chairBookings.push(action.payload)
            }
        },
        setChairBookeds: (state, { payload }) => {
            state.chairBookeds = [...state.chairBookeds, ...state.chairBookings]
            state.chairBookings = []
        },
    },
})

export const { actions: baiTapDatVeActions, reducer: baiTapDatVeReducer } = BTDatVeSlice

// immerJS
