import React, { useEffect, useState } from 'react'

const DemoUseEffect = () => {
    const [number, setNumber] = useState(1)
    const [like, setLike] = useState(1)

    // useEffect dùng để thực hiện các side effect :
    // call API, setState, clean up, tương tác với DOM

    // Cú pháp:
    // useEffect(() => {}, [])

    // UseEffect nhận vào 2 tham số : tham số 1: callback, tham số 2 (dependencies): []  có thể có hoặc ko
    // chạy sau khi component return (render xong UI)
    // useEffect luôn luôn chạy ít nhất 1 lần đối vs tất cả các trường hợp

    // TH1:  luôn luôn chạy mỗi khi component render => rất ít khi sử dụng
    useEffect(() => {
        console.log('useEffect TH1')
    })

    // TH2: Chạy duy nhất 1 lần sau khi component render
    useEffect(() => {
        console.log('usEffect TH2')
    }, [])

    //TH3: Chạy lại mỗi khi number thay đổi
    useEffect(() => {
        console.log('usEffect TH3')
    }, [number, like])

    //TH4: Thường đc sử dụng đẻ clean up component,
    //code trong hàm return sẽ chạy sau khi component bị hủy khỏi giao diện
    useEffect(() => {
        const time = setInterval(() => {
            console.log('Hello word')
        }, 1000)
        return () => {
            clearInterval(time)
        }
    }, [])

    console.log('Render')

    const [provinceId, setProvinceId] = useState()
    const [districts, setDistricts] = useState([])
    console.log('districts: ', districts)

    useEffect(() => {
        console.log('provinceId', provinceId)
        if (provinceId && provinceId !== 'none') {
            const districts = address.find((value) => value.id === provinceId).districts
            setDistricts(districts)
        }
    }, [provinceId])

    const address = [
        {
            province: 'Hồ Chí Minh',
            id: 'HCM',
            districts: [
                {
                    id: 'HCM01',
                    name: 'Quận 1',
                },
                {
                    id: 'HCM02',
                    name: 'Quận 2',
                },
                {
                    id: 'HCM03',
                    name: 'Quận 3',
                },
            ],
        },
        {
            province: 'Hà Nội',
            id: 'HN',
            districts: [
                {
                    id: 'HN01',
                    name: 'Quận Hoàn Kiếm',
                },
                {
                    id: 'HN02',
                    name: 'Quận Hai Bà Trưng',
                },
                {
                    id: 'HN03',
                    name: 'Quận Đống Đa',
                },
            ],
        },
    ]
    return (
        <div>
            <h1>DemoUseEffect</h1>
            <div className="mt-3">
                <p>Number: {number}</p>

                <button
                    className="btn btn-success mt-3"
                    onClick={() => {
                        setNumber(number + 1)
                    }}
                >
                    + Number
                </button>
            </div>
            <div className="mt-3">
                <p>Like: {like}</p>

                <button
                    className="btn btn-success mt-3"
                    onClick={() => {
                        setLike(like + 1)
                    }}
                >
                    + Like
                </button>
            </div>

            <div>
                <p>Example</p>
                <div>
                    <select
                        value={provinceId}
                        className="form-control"
                        onChange={(ev) => {
                            console.log(ev.target.value)
                            setProvinceId(ev.target.value)
                        }}
                    >
                        <option value="none">Vui lòng chọn tỉnh/thành phố</option>
                        {address.map((e) => (
                            <option value={e.id} key={e.id}>
                                {e.province}
                            </option>
                        ))}
                        {/* <option value="HN">Hà Nội</option> */}
                    </select>

                    <select className="form-control mt-3">
                        <option>Vui lòng chọn quận/huyện</option>
                        {districts?.map((district) => (
                            <option value={district.id} key={district.id}>
                                {district.name}
                            </option>
                        ))}
                    </select>
                </div>
            </div>
        </div>
    )
}

export default DemoUseEffect
