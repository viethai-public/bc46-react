import React, { memo } from 'react'

const Childrent = ({ like, handleLike }) => {
    console.log('childrent render')
    return (
        <div>
            <h2>Childrent</h2>
            <p className="mt-3">Like: {like}</p>
            <button className='btn btn-info' onClick={handleLike}>+ Like</button>
        </div>
    )
}

export default memo(Childrent)
