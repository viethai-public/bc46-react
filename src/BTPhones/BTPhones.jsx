//rafce
import React, { useState } from 'react'
import ProductList from './ProductList'
import data from './data.json'
import ProductDetail from './ProductDetail'
import GioHang from './GioHang'

const BTPhones = () => {
    // chi tiết sản phẩm
    const [prdDetail, setPrdDetail] = useState(data[0])

    // Danh sách giỏ hàng
    const [carts, setCarts] = useState([])

    console.log('carts: ', carts)

    const handlePrdDetail = (product) => {
        setPrdDetail(product)
    }

    const handleCart = (product) => {
        console.log('product: ', product)
        // coppy những sản phẩm đang có ở trong cart và thêm vào 1 product mới
        // spread operator

        // kiểm tra trong giỏ hàng đã tồn tại sản phẩm hay chưa
        // find => tìm thấy trả về đúng item thỏa mãn ĐK trong mảng, ko tìm thấy trả về undefined
        const item = carts.find((prd) => prd.maSP === product.maSP) // maSP = 1

        // const item = carts.find (prd => {
        //     return prd.maSP === product.maSP
        // })

        if (!item) {
            // ko tồn tại trong carts
            const newPrd = { ...product, soLuong: 1 }
            setCarts([...carts, newPrd])
        } else {
            // đã tồn tại trong carts
            const newPrd = { ...item, soLuong: item.soLuong + 1 }

            // tạo ra 1 mảng mới ko chứa sản phẩm có maSP = product.maSP truyền vào
            const newCart = carts.filter((cart) => cart.maSP !== item.maSP)

            setCarts([...newCart, newPrd])
        }

        // setCarts([...carts, {...product, soLuong: 1}])
    }

    const handleQuantity = (maSP, quantity) => {
        setCarts((currentState) => {
            const newCarts = currentState.map((cart) => {
                if (cart.maSP === maSP) {
                    return {
                        ...cart,
                        // soLuong: cart.soLuong + quantity <= 1 ? 1 : cart.soLuong + quantity,
                        soLuong: cart.soLuong + quantity || 1,
                    }
                }
                return cart
            })

            return newCarts
        })
    }

    // const handleDecreaQuantity = () => {}

    return (
        <div className="container mt-5">
            <div className="d-flex justify-content-between mb-5">
                <h1>BTPhones</h1>
                <button className="btn btn-danger" data-toggle="modal" data-target="#gioHang">
                    Giỏ hàng
                </button>
            </div>

            <ProductList data={data} handlePrdDetail={handlePrdDetail} handleCart={handleCart} />

            {/* Chi tiết sản phẩm */}
            <ProductDetail prdDetail={prdDetail} />

            {/* Giỏ hàng */}
            <GioHang carts={carts} handleQuantity={handleQuantity} />
        </div>
    )
}

export default BTPhones

// falsy - truethy
// falsy: undefined, NaN, 0, '', fale, null

// let a = undefined

// numbers = [1,2,3,4]
//numbersClone = [1,2,3]
