//rafce
import React, { useState } from 'react'
import movieList from './data.json'
import { useNavigate, generatePath, useSearchParams, useLocation } from 'react-router-dom'
import { PATH } from '../config/path'
import { generateSearchString } from '../utils/generateSearchString'
import qs from 'qs'

const BTMovie = () => {
    console.log({ movieList })
    const navigate = useNavigate()
    const [searchParams, setSearchParams] = useSearchParams()

    const { pathname } = useLocation()

    console.log('pathname: ', pathname)

    const searchQuery = Object.fromEntries(searchParams)
    console.log('searchQuery: ', searchQuery)

    const [searchValue, setSearchValue] = useState()

    console.log('searchValue: ', searchValue)

    // console.log('searchQuery: ', searchQuery.movieName)

    // console.log('searchParams: ', searchParams.get('movieName'))

    const findMovieList = () => {
        const data = movieList.filter((item) =>
            item.tenPhim.toLowerCase().includes(searchQuery.movieName.toLowerCase())
        )
        return data
    }

    const renderMovie = (arrMovie) => {
        return arrMovie.map((movie) => {
            const path = generatePath(PATH.movieDetail, { movieId: movie.maPhim })
            return (
                <div key={movie.maPhim} className="col-3 mt-3">
                    <div className="card">
                        <img
                            style={{ width: '250px', height: '350px' }}
                            src={movie.hinhAnh}
                            alt="..."
                        />
                        <div className="card-body" style={{ overflow: 'hidden' }}>
                            <p className="font-weight-bold">{movie.tenPhim}</p>
                            <p>{movie.moTa.substring(0, 50)}...</p>
                            <button
                                className="btn btn-outline-success mt-3"
                                onClick={() => {
                                    navigate(path)
                                }}
                            >
                                Detail
                            </button>
                        </div>
                    </div>
                </div>
            )
        })
    }

    // findMovieList()
    //JSON.stringify
    const queryString = qs.stringify(
        {
            // name: 'abc',
            age: undefined,
        },
        {
            addQueryPrefix: true,
        }
    )
    console.log('queryString: ', queryString)

    return (
        <div className="container">
            <h1>BTMovie</h1>
            <div>
                <input
                    className="form-control"
                    placeholder="Nhập tên phim"
                    value={searchValue}
                    defaultValue={searchQuery?.movieName}
                    onChange={(event) => {
                        // lấy giá trị user nhập vào input
                        // console.log(event.target.value)
                        setSearchValue(event.target.value)
                    }}
                />
                <button
                    className="btn btn-success mt-3"
                    onClick={() => {
                        const searchString = generateSearchString({
                            movieName: searchValue || undefined,
                        })
                        // setSearchParams({
                        //     movieName: undefined,
                        // })
                        navigate(pathname + searchString)
                    }}
                >
                    Tìm kiếm
                </button>
            </div>
            <div className="row">
                {/* {movieList.map((movie) => {
                    const path = generatePath(PATH.movieDetail, { movieId: movie.maPhim })
                    return (
                        <div key={movie.maPhim} className="col-3 mt-3">
                            <div className="card">
                                <img
                                    style={{ width: '250px', height: '350px' }}
                                    src={movie.hinhAnh}
                                    alt="..."
                                />
                                <div className="card-body" style={{ overflow: 'hidden' }}>
                                    <p className="font-weight-bold">{movie.tenPhim}</p>
                                    <p>{movie.moTa.substring(0, 50)}...</p>
                                    <button
                                        className="btn btn-outline-success mt-3"
                                        onClick={() => {
                                            navigate(path)
                                        }}
                                    >
                                        Detail
                                    </button>
                                </div>
                            </div>
                        </div>
                    )
                })} */}

                {renderMovie(searchQuery?.movieName ? findMovieList() : movieList)}
            </div>
        </div>
    )
}

export default BTMovie
