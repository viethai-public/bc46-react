import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { btFormActions } from '../storeToolkit/BTForm/slice'

const FormProduct = () => {
    // button nằm trong thẻ form nếu ko gắn thuộc tính type => mặc định nó sẽ là submit

    const [formData, setFormData] = useState()
    console.log('formData: ', formData)

    const [formError, setFormError] = useState()
    console.log('formError: ', formError)

    const dispatch = useDispatch()

    const { productEdit } = useSelector((state) => state.btForm)
    console.log('productEdit: ', productEdit)

    // const handleFormData = (name, value) => {
    //     setFormData({
    //         ...formData,
    //         [name]: value,
    //     })
    // }

    // currying function
    const handleFormData = () => (ev) => {
        const { name, value, title, minLength, max, validity } = ev.target
        console.log('name: ', name)
        console.log('validity: ', validity)

        let mess // undefined
        if (minLength !== -1 && !value.length) {
            mess = `Vui lòng nhập thông tin ${title}`
        } else if (value.length < minLength) {
            // validity.tooShort
            mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`
        } else if (value.length > max && max) {
            mess = `Chỉ được nhập tối đa ${max} ký tự`
        } else if (validity.patternMismatch && ['id', 'price'].includes(name)) {
            mess = `Vui lòng nhập ký tự là số`
        } else if (validity.patternMismatch && name === 'email') {
            mess = `Vui lòng nhập đúng email`
        }

        // console.log('mess: ', mess)
        setFormError({
            ...formError,
            [name]: mess,
        })

        setFormData({
            ...formData,
            [name]: mess ? undefined : value,
        })
    }

    useEffect(() => {
        if (!productEdit) return

        setFormData(productEdit)
    }, [productEdit])

    return (
        <form
            onSubmit={(event) => {
                // ngăn sự kiện re load khi submit form
                event.preventDefault()

                const elements = document.querySelectorAll('input')
                console.log('elements: ', elements)

                let formError = {}
                elements.forEach((ele) => {
                    let mess
                    const { name, value, title, minLength, max, validity } = ele

                    if (minLength !== -1 && !value.length) {
                        mess = `Vui lòng nhập thông tin ${title}`
                    } else if (value.length < minLength) {
                        // validity.tooShort
                        mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`
                    } else if (value.length > max && max) {
                        mess = `Chỉ được nhập tối đa ${max} ký tự`
                    } else if (validity.patternMismatch && ['id', 'price'].includes(name)) {
                        mess = `Vui lòng nhập ký tự là số`
                    } else if (validity.patternMismatch && name === 'email') {
                        mess = `Vui lòng nhập đúng email`
                    }

                    formError[name] = mess
                })

                console.log('formError: ', formError)
                let flag = false

                for (let key in formError) {
                    // console.log('value: ', value);
                    if (formError[key]) {
                        flag = true
                        break
                    }
                }

                if (flag) {
                    setFormError(formError)
                    return
                }
                if (productEdit) {
                    dispatch(btFormActions.updateProduct(formData))
                } else {
                    dispatch(btFormActions.addProduct(formData))
                }
            }}
            noValidate
        >
            <h2 className="px-2 py-4 bg-dark text-warning">Product Info</h2>
            <div className="form-group row">
                <div className="col-6">
                    <p>ID</p>
                    <input
                        type="text"
                        disabled={!!productEdit}
                        value={formData?.id}
                        className="form-control"
                        name="id"
                        title="id"
                        minLength={6}
                        max={20}
                        pattern="^[0-9]+$"
                        // maxLength={20}
                        // required
                        // onChange={(ev) => {
                        //     setFormData({
                        //         ...formData,
                        //         id: ev.target.value,
                        //     })
                        // }}
                        // onChange={(ev) => {
                        //     handleFormData('id', ev.target.value)
                        // }}
                        onChange={handleFormData()}
                    />
                    <p className="text-danger">{formError?.id}</p>
                </div>
                <div className="col-6">
                    <p>Image</p>
                    <input
                        value={formData?.image}
                        type="text"
                        className="form-control"
                        name="image"
                        title="image"
                        minLength={1}
                        onChange={handleFormData()}
                    />
                    <p className="text-danger">{formError?.image}</p>
                </div>
            </div>
            <div className="form-group row">
                <div className="col-6">
                    <p>Name</p>
                    <input
                        type="text"
                        value={formData?.name}
                        className="form-control"
                        name="name"
                        onChange={handleFormData()}
                    />
                </div>
                <div className="col-6">
                    <p>Product Type</p>
                    <input
                        type="text"
                        value={formData?.productType}
                        className="form-control"
                        name="productType"
                        onChange={handleFormData()}
                    />
                </div>
            </div>
            <div className="form-group row">
                <div className="col-6">
                    <p>Price</p>
                    <input
                        type="text"
                        value={formData?.price}
                        className="form-control"
                        name="price"
                        pattern="^[0-9]+$"
                        onChange={handleFormData()}
                    />
                    <p className="text-danger">{formError?.price}</p>
                </div>
                <div className="col-6">
                    <p>Description</p>
                    <input
                        type="text"
                        value={formData?.description}
                        className="form-control"
                        name="description"
                        onChange={handleFormData()}
                    />
                </div>
            </div>
            <div className="mt-3">
                {productEdit && <button className="btn btn-info ml-3">Update</button>}

                {!productEdit && <button className="btn btn-success">Create</button>}
            </div>
        </form>
    )
}

export default FormProduct
