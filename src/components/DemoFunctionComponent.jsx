// Tạo nhanh function Component : rafce

import React from 'react'

const DemoFunctionComponent = () => {
    return (
        <div>
            <h1>H1</h1>
            <div>DemoFunctionComponent</div>
        </div>
    )
}

export default DemoFunctionComponent
