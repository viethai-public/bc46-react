import React from 'react'
import Sidebar from '../components/Sidebar'
import { Outlet } from 'react-router-dom'
import BTMovie from '../BTMovie/BTMovie'

const MainLayout = () => {
    return (
        <div>
            <div className="row">
                {/* side bar */}
                <div className="col-2">
                    <Sidebar />
                </div>

                {/* content */}
                <div className="col-10">
                    <Outlet />
                </div>
            </div>
        </div>
    )
}

export default MainLayout
