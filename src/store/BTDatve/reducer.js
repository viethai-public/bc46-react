import { HANDLE_CHAIR_BOOKINGS, PAY } from './actionType'

const initialState = {
    chairBookings: [],
    chairBookeds: [],
}

export const btDatveReducer = (state = initialState, { type, payload }) => {
    console.log('payload: ', payload)
    switch (type) {
        case HANDLE_CHAIR_BOOKINGS: {
            const data = [...state.chairBookings]
            // kiểm tra ghế đc click nó đã tồn tại trong mảng ghế đang chọn hay ko
            const index = data.findIndex((value) => value.soGhe === payload.soGhe)

            if (index === -1) {
                data.push(payload)
            } else {
                data.splice(index, 1)
            }
            return { ...state, chairBookings: data }
        }
        case PAY: {
            const data = [...state.chairBookeds, ...state.chairBookings]
            return { ...state, chairBookeds: data, chairBookings: [] }
        }

        default:
            return state
    }
}
