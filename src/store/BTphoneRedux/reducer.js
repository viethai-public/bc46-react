import { HANDLEQUANTITY, HANDLE_CARTS, HANDLE_PRODUCTDETAIL } from './actionType'

const initialState = {
    productDetail: {
        maSP: 1,
        tenSP: 'VinSmart Live',
        manHinh: 'AMOLED, 6.2, Full HD+',
        heDieuHanh: 'Android 9.0 (Pie)',
        cameraTruoc: '20 MP',
        cameraSau: 'Chính 48 MP & Phụ 8 MP, 5 MP',
        ram: '4 GB',
        rom: '64 GB',
        giaBan: 5700000,
        hinhAnh: './images/phones/vsphone.jpg',
    },
    carts: [],
}

export const btPhoneReducer = (state = initialState, action) => {
    console.log('action: ', action)
    switch (action.type) {
        case HANDLE_PRODUCTDETAIL: {
            return { ...state, productDetail: { ...action.payload } }
        }
        case HANDLE_CARTS: {
            let newCarts = [...state.carts]

            // Tìm kiếm trong carts xem có sp trùng với sp gửi lên hay ko
            const index = state.carts.findIndex((prd) => prd.maSP === action.payload.maSP) // -1

            // nếu chưa tồn tại sp trong cart
            if (index === -1) {
                newCarts.push({
                    ...action.payload,
                    soLuong: 1,
                })
            } else {
                newCarts[index].soLuong += 1
            }

            return { ...state, carts: newCarts }
        }
        case HANDLEQUANTITY: {
            let newCarts = [...state.carts]
            newCarts = newCarts.map((cart) => {
                if (cart.maSP === action.payload.maSP) {
                    return {
                        ...cart,
                        soLuong: cart.soLuong + action.payload.quantity || 1,
                    }
                }
                return cart
            })

            return { ...state, carts: newCarts }
        }
        default:
            return state
    }
}
